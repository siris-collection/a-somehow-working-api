<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: DELETE');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, 
              Access-Control-Allow-Methods, Authorization');

require '../../config/Database.php';
require '../../models/Post.php';

//    Instantiate DB and Connect
$database = new Database();
$db = $database->connect();

//    Instantiate blog post object
$post = new Post($db);

//get data
$data = json_decode(file_get_contents("php://input"));

$post->id = $data->id;

if ($post->delete_post()) {
    echo json_encode(
        array('message' => 'Post deleted')
    );
} else {
    echo json_encode(
        array('message' => 'Post Not deleted')
    );
}