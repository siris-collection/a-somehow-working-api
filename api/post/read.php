<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

require '../../config/Database.php';
require '../../models/Post.php';

//    Instantiate DB and Connect
$database = new Database();
$db = $database->connect();


//    Instantiate blog post object
$post = new Post($db);

$result = $post->read();

$rowNum = $result->rowCount();

if ($rowNum > 0) {
    $posts_array = array();
    $posts_array['data'] = array();

    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {


        $post_item = array('id' => $row['id'], 'title' => $row['title'], 'body' => html_entity_decode($row['body']), 'author' => $row['author'],
        'category_id' => $row['category_id']);

    array_push($posts_array['data'], $post_item);
}

//    Turn to json
    echo json_encode($posts_array);
//    file_put_contents("allPosts.json", json_encode($posts_array));

} else {
    array('message' => 'No Posts Found');
}