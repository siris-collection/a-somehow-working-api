<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

require '../../config/Database.php';
require '../../models/Post.php';

//    Instantiate DB and Connect
$database = new Database();
$db = $database->connect();

//    Instantiate blog post object
$post = new Post($db);

//    get id
$post->id = $_GET['id'] ?? die();

//    get post
$post->read_single();

$post_array = array(
    'id' => $post->id,
    'title' => $post->title,
    'body' => $post->body,
    'category_id' => $post->category_id,
    'created_at' => $post->created_at
);

//    turn into json

print_r(json_encode($post_array));

