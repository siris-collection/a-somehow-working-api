<?php

class Post
{
//    DB stuff
    private $conn;
    private $table = 'sirivat2';

//    Post Properties
    public $id;
    public $category_id;
    public $category_name;
    public $title;
    public $body;
    public $author;
    public $created_at;

//    Constructer with DB
    public function __construct($db)
    {
        $this->conn = $db;
    }

//    Get Posts

    public function read()
    {
        $query = 'SELECT 
                 id, 
                 category_id, 
                 title, 
                 body, 
                 author, 
                 created_at    
                 FROM 
                 sirivat2
                 ORDER BY created_at DESC';

        //    Prepare statement
        $stmt = $this->conn->prepare($query);

//        Execute query
        $stmt->execute();

        return $stmt;
    }

//    Get single post
    public function read_single()
    {
        $query = 'SELECT 
                id, 
                category_id, 
                title, 
                body, 
                author, 
                created_at    
                FROM 
                sirivat2
                WHERE id = ?';

        $stmt = $this->conn->prepare($query);
        $stmt->bindparam(1, $this->id);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

//        Set properties
        $this->title = $row['title'];
        $this->body = $row['body'];
        $this->author = $row['author'];
        $this->category_id = $row['category_id'];
        $this->created_at = $row['created_at'];

    }

//    create a new post
    public function create_post()
    {
        $query = 'INSERT INTO sirivat2
                  (title, body, author, category_id)
                  VALUES (:title, :body, :author, :category_id)';

        $this->title = htmlspecialchars($this->title);
        $this->body = htmlspecialchars($this->body);
        $this->author = htmlspecialchars($this->author);
        $this->category_id = ($this->category_id);

        //    bind param
        $stmt = $this->conn->prepare($query);
        $stmt->bindparam(':title', $this->title);
        $stmt->bindparam(':body', $this->body);
        $stmt->bindparam(':author', $this->author);
        $stmt->bindparam(':category_id', $this->category_id);

//      execute
        if ($stmt->execute()) {
            return true;
        } else {
            printf("Error: ", $stmt->error);
            return false;
        }
    }

    //    update a post
    public function update_post()
    {
        $query = 'UPDATE sirivat2
                  SET title = :title, body = :body, author = :author, category_id = :category_id
                  WHERE id = :id';

        $this->title = htmlspecialchars($this->title);
        $this->body = htmlspecialchars($this->body);
        $this->author = htmlspecialchars($this->author);
        $this->category_id = $this->category_id;
        $this->id = $this->id;

        //    bind param
        $stmt = $this->conn->prepare($query);
        $stmt->bindparam(':title', $this->title);
        $stmt->bindparam(':body', $this->body);
        $stmt->bindparam(':author', $this->author);
        $stmt->bindparam(':category_id', $this->category_id);
        $stmt->bindparam(':id', $this->id);


//      execute
        if ($stmt->execute()) {
            return true;
        } else {
            printf("Error: ", $stmt->error);
            return false;
        }
    }

//    delete post
    public function delete_post()
    {
        $query = 'DELETE FROM sirivat2 WHERE id = :id';

        $stmt = $this->conn->prepare($query);
        $stmt->bindparam(':id', $this->id);

        if ($stmt->execute()) {
            return true;
        } else {
            printf("Error: ", $stmt->error);
            return false;
        }
    }

}