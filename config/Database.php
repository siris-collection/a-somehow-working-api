<?php
class Database
{
//    DB params
//    private $host = 'devdb.intern.ebroot.de';
//    private $db_name = 'company';

    private $dsn = 'pgsql:host=devdb.intern.ebroot.de; dbname=company';
    private $username = 'sasanasuwan';
    private $password = '$Ok9i1&7VM';
    private $conn;

//    DB connection
    public function connect() {
        $this->conn = null;

        try {
            $this->conn = new PDO($this->dsn, $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e){
            echo "Connection error: " . $e->getMessage();
        }

        return $this->conn;
    }
}